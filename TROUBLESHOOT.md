# Troubleshoot

## SElinux

During the initial phase, haproxy failed to start the stats backend after installation. `journalctl -xe` reported this problems.
```
Dec 29 06:50:04 lb1 haproxy[4770]: [ALERT] 363/065004 (4770) : Starting frontend stats: cannot bind socket (Permission denied) [0.0.0.0:8334]
Dec 29 06:50:04 lb1 haproxy[4770]: [ALERT] 363/065004 (4770) : [/usr/sbin/haproxy.main()] Some protocols failed to start their listeners! Exiting.
```

This is due to the enforced SElinux and can be debugged. To get semanage and sepolicy commands, install:
```
$ yum install -y setroubleshoot-server selinux-policy-devel
```

After the installation of those tools, the logging changes to
```
Dec 29 07:18:12 lb1 setroubleshoot[4496]: SELinux is preventing /usr/sbin/haproxy from name_bind access on the tcp_socket port 8404
```

List well-known ports
```
sepolicy network -t http_port_t
```

- [RHEL7: Use SELinux port labelling to allow services to use non-standard ports. - CertDepot](https://www.certdepot.net/rhel7-use-selinux-port-labelling/)

