#!/bin/bash

#
# Build HAproxy
#

# Build dependencies
sudo yum install -y git vim

# get repo
git clone https://github.com/DBezemer/rpm-haproxy.git 
cd ./rpm-haproxy
make

sudo yum install -y createrepo rng-tools
sudo rngd -r /dev/urandom
sudo mkdir /usr/share/repo
sudo createrepo /usr/share/repo


sudo rpm -i rpmbuild/RPMS/x86_64/haproxy-2.2.1-1.el7.x86_64.rpm
sudo systemctl enable haproxy.service

sudo rm /etc/haproxy/haproxy.cfg
sudo ln -s /vagrant/haproxy.cfg /etc/haproxy/haproxy.cfg
haproxy -c -V -f /etc/haproxy/haproxy.cfg

sudo systemctl reload haproxy.service


#
# Setup Docker environment
#

# Installing Docker Compose
# Check the current release and if necessary (https://github.com/docker/compose/releases)
sudo curl -L "https://github.com/docker/compose/releases/download/1.26.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
docker-compose --version

# Installing Docker
sudo yum check-update
curl -fsSL https://get.docker.com/ | sh
sudo systemctl start docker
sudo systemctl enable docker
sudo usermod -aG docker $(whoami)
docker -v

# Run ruby webservers
cd /vagrant
docker-compose up -d