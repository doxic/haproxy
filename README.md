# HAProxy

Interactive docker console to test
```
sudo docker run --volume ${PWD}/files:/srv -it rpmbuild/centos7 /bin/bash
```

Build rpm
```
sudo docker run --volume ${PWD}/files:/srv -t rpmbuild/centos7
```

### Syslog

Based on management document from HAProxy, use of UNIX socket send logs is not recommended due to multiple issues. We should choose a facility that is not used by other daemons, mostly "local0", "local1" and "local2" are used in examples.


| Code | Severity |                                                                                                             Description                                                                                                              |
|------|----------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|    0 | emerg    | Errors such as running out of operating system file descriptors.                                                                                                                                                                     |
|    1 | alert    | Some rare cases where something unexpected has happened, such as being unable to cache a response.                                                                                                                                   |
|    2 | crit     | Not used.                                                                                                                                                                                                                            |
|    3 | error    | Errors such as being unable to parse a map file, being unable to parse the HAProxy configuration file, and when an operation on a stick table fails.                                                                                 |
|    4 | warning  | Certain important, but non-critical, errors such as failing to set a request header or failing to connect to a DNS nameserver.                                                                                                       |
|    5 | notice   | Changes to a server’s state, such as being UP or DOWN or when a server is disabled. Other events at startup, such as starting proxies and loading modules are also included. Health check logging, if enabled, also uses this level. |
|    6 | info     | TCP connection and HTTP request details and errors.                                                                                                                                                                                  |
|    7 | debug    | Custom Lua code that logs debug messages.                                                                                                                                                                                            |


- [SELinux Policy Modules Packaging Draft - Fedora Project Wiki](https://fedoraproject.org/wiki/SELinux_Policy_Modules_Packaging_Draft)
- [Introduction to HAProxy Logging - HAProxy Technologies](https://www.haproxy.com/blog/introduction-to-haproxy-logging/)
- [How To Configure HAProxy Logging with Rsyslog on CentOS 8 [Quickstart] | DigitalOcean](https://www.digitalocean.com/community/tutorials/how-to-configure-haproxy-logging-with-rsyslog-on-centos-8-quickstart)
- [HAProxy Management Guide](https://www.haproxy.org/download/2.3/doc/management.txt)


## Installation

```shell
sudo yum install -y make gcc perl pcre-devel zlib-devel systemd-devel vim wget

OPENSSL_VERSION=1.1.1i
wget -O /tmp/openssl.tar.gz https://www.openssl.org/source/openssl-${OPENSSL_VERSION}.tar.gz
tar -zxvf /tmp/openssl.tar.gz -C /tmp
cd /tmp/openssl-${OPENSSL_VERSION}
export STATICLIBSSL=/tmp/staticlibssl
./config --prefix=$STATICLIBSSL no-shared
make -j $(nproc) && make install_sw

MAINVERSION=2.2
VERSION=$(wget -qO- http://git.haproxy.org/git/haproxy-${MAINVERSION}.git/refs/tags/ | sed -n 's:.*>\(.*\)</a>.*:\1:p' | sed 's/^.//' | sort -rV | head -1)
echo $VERSION

wget -O /tmp/haproxy.tgz http://www.haproxy.org/download/${MAINVERSION}/src/haproxy-${VERSION}.tar.gz
tar -zxvf /tmp/haproxy.tgz -C /tmp

cd /tmp/haproxy-${VERSION}

make -j $(nproc) TARGET=linux-glibc USE_OPENSSL=1 USE_OPENSSL=1 SSL_INC=$STATICLIBSSL/include SSL_LIB=$STATICLIBSSL/lib USE_ZLIB=1 USE_PCRE=1 USE_SYSTEMD=1 ADDLIB=-lpthread EXTRA_OBJS="contrib/prometheus-exporter/service-prometheus.o"

sudo make install
```

```shell
cd contrib/systemd/
make
sudo cp haproxy.service /usr/lib/systemd/system/
sudo systemctl daemon-reload
sudo systemctl enable haproxy
```

haproxy user and groupd
```
sudo mkdir -p /var/lib/haproxy/
getent group haproxy >/dev/null || \
       sudo groupadd -g 188 -r haproxy
getent passwd haproxy >/dev/null || \
       sudo useradd -u 188 -r -g haproxy -d /var/lib/haproxy \
       -s /sbin/nologin -c "haproxy" haproxy
```

haproxy config
```
sudo mkdir -p /etc/haproxy/
cp /vagrant/files/haproxy.cfg /etc/haproxy/haproxy.cfg
haproxy -c -V -f /etc/haproxy/haproxy.cfg
sudo systemctl start haproxy
```

Syslog
```
sudo mkdir -p /var/log/haproxy
cp /vagrant/files/haproxy.syslog /etc/rsyslog.d/haproxy.conf
sudo systemctl restart rsyslog.service
```

Logrotate
```
cp /vagrant/files/haproxy.logrotate /etc/logrotate.d/logrotate
```

- Monitor-URI for healt-checks over `http://10.0.0.20/monitoruri`
- Stats over `http://10.0.0.20:8404/stats`
- Prometheus metrics over `http://10.0.0.20:8404/metrics`


## Dynamic Configuration with the HAProxy Runtime API
Testing interactive commands
```
socat readline /var/run/haproxy.sock
```

For accessing the Runtime API from script the following command could be used:
```
$ echo "help" | socat stdio /var/run/haproxy.sock
```

## Ressources
- [haproxy-2.3.2-1.fc34 | Build Info | koji](https://koji.fedoraproject.org/koji/buildinfo?buildID=1656516)