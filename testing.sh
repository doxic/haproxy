#!/bin/bash

# Terminate with `pkill python`

cat << 'EOF' > http.sh
#!/bin/bash
WEBROOT=${1:-web1}
PORT=${2:-8008}

pushd ${WEBROOT}
python -m SimpleHTTPServer ${PORT} > log.txt 2>&1 
popd
EOF

chmod +x http.sh

for WEBSERVICE in 1 2
do
	mkdir -p web"${WEBSERVICE}"
    cat << EOF > web"${WEBSERVICE}"/index.html
<html>
<header><title>web ${WEBSERVICE}</title></header>
<body>
    <h1>web ${WEBSERVICE}</h1>
</body>
</html>
EOF
    ./http.sh web"${WEBSERVICE}" $(( $WEBSERVICE+8007 )) &
    
done
