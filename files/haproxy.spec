# RPM specfile for the HAProxy Load Balancer

%undefine _disable_source_fetch

# global
%global _hardened_build 1
%global _performance_build 1

# selinux
%global selinux_variants targeted
%global modulename rsyslog-haproxy

# haproxy version
%define haproxy_major 2.3
%define haproxy_minor 2

# library
%define openssl_version 1.1.1i
%define package_release 1i

# haproxy settings
%define haproxy_user    haproxy
%define haproxy_group   %{haproxy_user}
%define haproxy_homedir %{_localstatedir}/lib/haproxy
%define haproxy_confdir %{_sysconfdir}/haproxy
%define haproxy_datadir %{_datadir}/haproxy

# package information
Name:           haproxy
Version:        %{haproxy_major}.%{haproxy_minor}
Release:        %{package_release}%{?dist}
Summary:        HAProxy reverse proxy for high availability environments

License:        GPLv2+

URL:            http://www.haproxy.org

# sources
Source0:        %{url}/download/%{haproxy_major}/src/haproxy-%{version}.tar.gz
Source1:        https://www.openssl.org/source/openssl-%{openssl_version}.tar.gz
Source2:        %{name}.cfg
Source3:        %{name}.logrotate
Source4:        %{name}.sysconfig
Source5:        halog.1
Source6:        %{name}.syslog
Source7:        %{modulename}.te


# build requirements
BuildRequires:  gcc
BuildRequires:  lua-devel
BuildRequires:  pcre-devel
BuildRequires:  zlib-devel
BuildRequires:  systemd-devel
BuildRequires:  systemd
BuildRequires:  checkpolicy, selinux-policy-devel

# requirements
Requires(pre):      shadow-utils
Requires:           selinux-policy
%{?systemd_requires}
Requires(post):   /usr/sbin/semodule, /sbin/fixfiles, %{name}
Requires(postun): /usr/sbin/semodule

# description
%description
HAProxy Load Balancer with recent OpenSSL and Prometheus metrics service.

%prep
%setup -q -n %{name}-%{version}
mkdir SELinux
cp -p %{SOURCE7} SELinux

%build

# make selinux policy module
pushd SELinux
for selinuxvariant in %{selinux_variants}
do
  %{__make} NAME=${selinuxvariant} -f /usr/share/selinux/devel/Makefile
  mv %{modulename}.pp %{modulename}.pp.${selinuxvariant}
  %{__make} NAME=${selinuxvariant} -f /usr/share/selinux/devel/Makefile clean
done
popd

# make openssl
mkdir -p %{_tmppath}/local
tar -xzvf %{SOURCE1} -C %{_tmppath}/local
pushd %{_tmppath}/local/openssl-%{openssl_version}
# Disables TLS<1.2
# Disables RC4
./config --prefix=%{_tmppath}/local \
  no-shared \
  no-ssl2 no-ssl3 no-tls1 no-tls1_1 \
  no-weak-ssl-ciphers \
  -DPURIFY
%{__make} -j$(nproc) %{?_smp_mflags}
%{__make} all install_sw
popd

# make haproxy
regparm_opts=
%ifarch %ix86 x86_64
regparm_opts="USE_REGPARM=1"
%endif

# [About OpenSSL 1.1.1c problems when building haproxy · Issue #204 · haproxy/haproxy](https://github.com/haproxy/haproxy/issues/204)
%{__make} -j$(nproc) %{?_smp_mflags} ARCH=%{_target_cpu} TARGET=linux-glibc \
  USE_SYSTEMD=1 \
  USE_OPENSSL=1 \
  USE_ZLIB=1 \
  USE_PCRE=1 \
  SSL_LIB=%{_tmppath}/local/lib \
  SSL_INC=%{_tmppath}/local/include \
  ${regparm_opts} \
  ADDINC='%{optflags}' \
  ADDLIB='%{__global_ldflags} -pthread' \
  EXTRA_OBJS='contrib/prometheus-exporter/service-prometheus.o'

# make halog
%{__make} -C contrib/halog OPTIMIZE="%{optflags} %{__global_ldflags}"

# make iprange
%{__make} -C contrib/iprange OPTIMIZE="%{optflags} %{__global_ldflags}"

# make systemd service
sed -e 's:@SBINDIR@:'%{_sbindir}':' contrib/systemd/haproxy.service.in > haproxy.service

%pre
getent group %{haproxy_group} >/dev/null || \
    groupadd -r %{haproxy_group}
getent passwd %{haproxy_user} >/dev/null || \
    useradd -r -g %{haproxy_user} -d %{haproxy_homedir} \
    -s /sbin/nologin -c "haproxy" %{haproxy_user}
exit 0

%install
%{__make} install-bin DESTDIR=%{buildroot} PREFIX=%{_prefix} TARGET="linux2628"
%{__make} install-man DESTDIR=%{buildroot} PREFIX=%{_prefix}

%{__install} -p -D -m 0644 haproxy.service %{buildroot}%{_unitdir}/%{name}.service
%{__install} -p -D -m 0644 %{SOURCE2} %{buildroot}%{haproxy_confdir}/%{name}.cfg
%{__install} -p -D -m 0644 %{SOURCE3} %{buildroot}%{_sysconfdir}/logrotate.d/%{name}
%{__install} -p -D -m 0644 %{SOURCE4} %{buildroot}%{_sysconfdir}/sysconfig/%{name}
%{__install} -p -D -m 0644 %{SOURCE5} %{buildroot}%{_mandir}/man1/halog.1
%{__install} -p -D -m 0644 %{SOURCE6} %{buildroot}%{_sysconfdir}/rsyslog.d/%{name}.conf

%{__install} -d -m 0755 %{buildroot}%{haproxy_homedir}
%{__install} -d -m 0755 %{buildroot}%{haproxy_homedir}/dev
%{__install} -d -m 0755 %{buildroot}%{haproxy_datadir}
%{__install} -d -m 0755 %{buildroot}%{_bindir}

# log files
%{__install} -d -m 0755 %{buildroot}%{_localstatedir}/log/%{name}
touch %{buildroot}%{_localstatedir}/log/%{name}/access.log
touch %{buildroot}%{_localstatedir}/log/%{name}/error.log
touch %{buildroot}%{_localstatedir}/log/%{name}/status.log

%{__install} -p -m 0755 ./contrib/halog/halog %{buildroot}%{_bindir}/halog
%{__install} -p -m 0755 ./contrib/iprange/iprange %{buildroot}%{_bindir}/iprange
%{__install} -p -m 0644 ./examples/errorfiles/* %{buildroot}%{haproxy_datadir}

for httpfile in $(find ./examples/errorfiles/ -type f) 
do
    %{__install} -p -m 0644 $httpfile %{buildroot}%{haproxy_datadir}
done

%{__rm} -rf ./examples/errorfiles/

find ./examples/* -type f ! -name "*.cfg" -exec %{__rm} -f "{}" \;

for textfile in $(find ./ -type f -name '*.txt')
do
    %{__mv} $textfile $textfile.old
    iconv --from-code ISO8859-1 --to-code UTF-8 --output $textfile $textfile.old
    %{__rm} -f $textfile.old
done

# selinux
pushd SELinux
for selinuxvariant in %{selinux_variants}
do
  install -d %{buildroot}%{_datadir}/selinux/${selinuxvariant}
  install -p -m 644 %{modulename}.pp.${selinuxvariant} \
    %{buildroot}%{_datadir}/selinux/${selinuxvariant}/%{modulename}.pp
done
popd

/usr/sbin/hardlink -cv %{buildroot}%{_datadir}/selinux

%post
# selinux
for selinuxvariant in %{selinux_variants}
do
  /usr/sbin/semodule -s ${selinuxvariant} -i \
    %{_datadir}/selinux/${selinuxvariant}/%{modulename}.pp &> /dev/null || :
done
/sbin/fixfiles -R %{name} restore || :

%systemd_post %{name}.service
systemctl reload-or-try-restart rsyslog.service

%preun
%systemd_preun %{name}.service

%postun
if [ $1 -eq 0 ] ; then
  for selinuxvariant in %{selinux_variants}
  do
    /usr/sbin/semodule -s ${selinuxvariant} -r %{modulename} &> /dev/null || :
  done
fi
%systemd_postun_with_restart %{name}.service
systemctl reload-or-try-restart rsyslog.service


%files
%doc doc/* examples/*
%doc CHANGELOG README ROADMAP VERSION
%license LICENSE
%dir %{haproxy_homedir}
%dir %{haproxy_homedir}/dev
%dir %{haproxy_confdir}
%dir %{haproxy_datadir}
%{haproxy_datadir}/*
%config(noreplace) %{haproxy_confdir}/%{name}.cfg
%config(noreplace) %{_sysconfdir}/logrotate.d/%{name}
%config(noreplace) %{_sysconfdir}/sysconfig/%{name}
%config(noreplace) %{_sysconfdir}/rsyslog.d/%{name}.conf
%{_unitdir}/%{name}.service
%{_sbindir}/%{name}
%{_bindir}/halog
%{_bindir}/iprange
%{_mandir}/man1/*

# logfiles should be owned by the package but not installed
%ghost %{_localstatedir}/log/%{name}/access.log
%ghost %{_localstatedir}/log/%{name}/error.log
%ghost %{_localstatedir}/log/%{name}/status.log

# selinux
%{_datadir}/selinux/*/%{modulename}.pp

%changelog
* Mon Dec 29 2020 Dominic Ruettimann <dominic.ruettimann@gmail.com> - haproxy-2.3.2-1i.el7.centos
- Initial packaging based on Fedora and Matouš Jan Fialka
